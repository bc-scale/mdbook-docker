FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.17.0

RUN apk add --no-cache "chromium"

RUN rm -rf "/usr/local/bin"
COPY bin "/usr/local/bin"
